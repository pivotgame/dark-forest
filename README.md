# README #
Добро пожаловать на Bitbucket страничку игры Dark Forest!
Данная игра разрабатывается исключительно в качестве хобби и может свободно распространятся по интернету как есть.
Единственно условие - Обязательно указывать ссылку на первоисточник, т.е. на данную страничку.

# ССЫЛКИ#
* **[Dark Forest v0.0.1.20112015](https://bitbucket.org/pivotgame/dark-forest/src/a8b4859874cc0e583d6c41bcb59a70704b70e2dc/DarkForest%200.0.1.20112015/?at=master)** | [Все сборки](https://bitbucket.org/pivotgame/dark-forest/src/74714564ae5d0a53fbb2759d680c6d3eb40e5074?at=master)
* [Официальный блог](https://pivotgame.wordpress.com/)
* Автор: [Написать письмо](mailto:vladstrogonov@outlook.com) | [Вконтакте](http://vk.com)